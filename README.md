## Patiner Montréal for Android - Daily conditions of outdoor rinks

[![Patiner Montréal][img_ic_launcher]][link_patinoires_playstore]

### Enjoy the Winter!

Patiner Montréal provides you with daily updated information about conditions and services of Montréal's outdoor rinks.

Patiner Montréal for Android is an open source project to promote Open Data in Montréal. With the help of an other Montréal Ouvert [project][link_gh_patiner_mtl], the app retrieves information published by the city to bring it to your phone.

This app is powered by The City of Montréal [Open Data Portal][link_od_portal], in compliance with its Open Data [license][link_od_license]. Additional source: [City of Dorval][link_dorval_portal] Leisure and culture Portal.

## Features
* Skating and Hockey rinks.
* Dails conditions.
* Sort by distance.
* Offline mode.
* Rinks location map.
* Favorites list.
* Bilingual app, English/Français.
* Search and autocomplete.
* Free app!

## Links

* [Website][link_patinoires_website]
* [Privacy policy][link_patinoires_policy]
* [Patiner Montréal on Google Play][link_patinoires_playstore]

[![Android app on Google Play][img_playstore_badge]][link_patinoires_playstore]

## Credits

* Android app developed by [Mudar Noufal][link_mudar_ca] &lt;<mn@mudar.ca>&gt;.
* Art Direction &amp; Graphic Design by [Noémie Darveau][link_nofolio].
* Project done in collaboration with [Montréal ouvert][link_mtl_ouvert]. Many thanks to [James McKinney][link_gh_jpmckinney] (open data API), [Jonathan Brun][link_jbrun] &amp; [Daniel Mireault][link_dmireault].

The Android app includes libraries and derivative work of the following projects:

* [AOSP][link_lib_aosp] &copy; The Android Open Source Project.
* [Android Support Library v4][link_lib_supportv4] &copy; The Android Open Source Project.
* [Android Asset Studio][link_lib_ui_utils] &copy; Google Inc, used to create icons assets.

These three projects are all released under the [Apache License v2.0][link_apache].

## Code license

    Patiner Montréal for Android.
    Information about outdoor rinks in the city of Montréal: conditions,
    services, contact, map, etc.

    Copyright (C) 2010 Mudar Noufal <mn@mudar.ca>

    This file is part of Patiner Montréal for Android.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

[![Android app on Google Play][img_promo]][link_patinoires_playstore]

[img_ic_launcher]: http://patinoires.mudar.ca/img/ic_launcher.png
[img_playstore_badge]: http://patinoires.mudar.ca/img/en_app_rgb_wo_60.png
[img_promo]: http://patinoires.mudar.ca/img/patiner_montreal_promo_600x293.png
[link_patinoires_playstore]: https://play.google.com/store/apps/details?id=ca.mudar.patinoires
[link_patinoires_website]: http://patinoires.mudar.ca/
[link_patinoires_policy]: http://patinoires.mudar.ca/policy.html
[link_gh_patiner_mtl]: https://github.com/opennorth-archive/patinermontreal.ca
[link_od_portal]: http://donnees.ville.montreal.qc.ca/
[link_od_license]: http://donnees.ville.montreal.qc.ca/licence-texte-complet
[link_dorval_portal]: http://loisirs.ville.dorval.qc.ca/fr/sportifs/arenas-patinoires
[link_mudar_ca]: http://www.mudar.ca/
[link_nofolio]: http://www.nofolio.com/
[link_mtl_ouvert]: http://montrealouvert.net/donnees-ouvertes-questions-frequemment-demandees/?lang=en
[link_gh_jpmckinney]: https://github.com/jpmckinney
[link_jbrun]: http://www.jonathanbrun.com/
[link_dmireault]: http://www.behance.net/dmireault
[link_gpl]: http://www.gnu.org/licenses/gpl.html
[link_lib_aosp]: http://source.android.com/
[link_lib_supportv4]: http://developer.android.com/tools/support-library/
[link_lib_ui_utils]: http://code.google.com/p/android-ui-utils/
[link_apache]: http://www.apache.org/licenses/LICENSE-2.0
